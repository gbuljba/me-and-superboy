﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{

    #region variables

    //linija koja crta
    private LineRenderer lineRenderer;

    //elementi za crtanje: counter = povećavajuća mjera, koliko se linija pomaknula. dist = udaljenost. x = nešto, nisam previše proučio
    private float counter, dist, x;
    //vektori koji označavaju počku početka, kraja i točka trenutne pozicije između A i B
    private Vector3 pointA, pointB, pointAlongLine;

    //brzina crtanja linije + debljina linije
    public float lineDrawSpeed = 6f;
    public float lineWidth = 0.1f;


    //objekti među kojima će se crtati linija
    public GameObject[] dotsToConnect;

    //positionNum je counter od kojeg elementa trenutno ide linija. Max = broj elemenata u dotsToConnect
    private int positionNum = 0;
    private int max;

    //meshrenderer - za mijenjanje boja pojedinih elemenata/točaka, radi raspoznavanja koje da se spoje
    private MeshRenderer selected;
    //samo ih stavljam tako da pri pozivu metode paint ne moram pisati navodne znakove
    private string red = "red";
    private string green = "green";

    //označava da je vrijeme za crtanje linije, događa pri svakom pozivu update()
    private bool drawingTime;

    #endregion

    // Use this for initialization
    void Start()
    {

        //inicijalizacija linije
        lineRenderer = GetComponent<LineRenderer>();
        //pozicija linije se "postavlja" automatski (sve točke) pri bojanju prve točke zeleno

        max = dotsToConnect.Length;

        //LineRenderer mora imati broj točaka spajanja (između kojih se crta linija) jednak [sve točke]+1 (dva put se koristi prva točka)
        lineRenderer.positionCount = max + 1;

        //debljina crte
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = lineWidth;

        //pretvaram početnu točku crveno - označava da od nje počinje crtanje
        paint(red);
        drawingTime = false;
    }

    // Update is called once per frame
    void Update()
    {

        //drawingtime - dok linija nije došla do sljedeće točke pomiči ju, kad dođe bojaj zeleno i iduću crveno
        if (drawingTime)
        {
            if (counter < dist)
            {
                counter += .1f / lineDrawSpeed;
                x = Mathf.Lerp(0, dist, counter);
                pointAlongLine = x * Vector3.Normalize(pointB - pointA) + pointA;
                lineRenderer.SetPosition(positionNum, pointAlongLine);
            }

            //provjera da li je linija došla na sljedeću točku
            if (positionNum < max)
            {
                if (lineRenderer.GetPosition(positionNum) == dotsToConnect[positionNum].transform.position)
                //obojaj konačnu točku zeleno i sljedeću crveno
                {
                    paint(green);
                    //došao sam do "zadnje" točke - prvu točku ponovno crveno obojati
                    if (positionNum < max)
                    {
                        paint(red);
                    }
                    else
                    {
                        //ručno obojati prvi element crveno
                        selected = dotsToConnect[0].GetComponent<MeshRenderer>();
                        selected.material.color = new Color(255, 0, 0, 1);
                    }
                    drawingTime = false;
                }
            }
            //provjera da li se linija vratila na početnu točku
            else if (positionNum == max)
            {
                if (lineRenderer.GetPosition(positionNum) == dotsToConnect[0].transform.position)
                {
                    //ručno bojanje zeleno
                    selected = dotsToConnect[0].GetComponent<MeshRenderer>();
                    selected.material.color = new Color(0, 255, 0, 1);
                    positionNum++;
                    drawingTime = false;
                }
            }
        }

        //Da li je input klik miša
        if (Input.GetMouseButtonDown(0))
        {
            //ignoriram klik miša dok se crtanje odvija (i poseban slučaj kada su sve točke spojene)
            if (drawingTime || positionNum == max + 1)
            {
                return;
            }
            //očitavanje raycast-a: provjera je li početna točka kliknuta
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            //provjera - da li je kliknuta početna točka
            if (positionNum < max && hit.collider == dotsToConnect[positionNum].GetComponent<Collider2D>())
            {
                //kliknut je ispravno - stavi counter za liniju na nula
                counter = 0;

                if (positionNum > 0)
                //jedna zelena je točka + sljedeća je kliknuta - izračunaj kolika je duljina crtanja
                {
                    drawingTime = true;
                    dist = Vector3.Distance(
                        dotsToConnect[positionNum - 1].transform.position,
                        dotsToConnect[positionNum].transform.position
                    );
                    pointA = dotsToConnect[positionNum - 1].transform.position;
                    pointB = dotsToConnect[positionNum].transform.position;
                }
                else
                {
                    //obojaj početnu točku u zeleno i sljedeću u crveno
                    paint(green);
                    paint(red);
                }

            }
            else if (positionNum == max && hit.collider == dotsToConnect[0].GetComponent<Collider2D>())
            {
                counter = 0;
                dist = Vector3.Distance(
                           dotsToConnect[max - 1].transform.position,
                           dotsToConnect[0].transform.position
                );
                pointA = dotsToConnect[max - 1].transform.position;
                pointB = dotsToConnect[0].transform.position;

                drawingTime = true;
                //paint(green);
                //lineRenderer.SetPosition(max, dotsToConnect[0].transform.position);
            }
        }
    }

    private void paint(string colour)
    {
        if (colour.Equals("red"))
        {
            selected = dotsToConnect[positionNum].GetComponent<MeshRenderer>();
            selected.material.color = new Color(255, 0, 0, 1);

        }
        if (colour.Equals("green"))
        {
            selected = dotsToConnect[positionNum].GetComponent<MeshRenderer>();
            selected.material.color = new Color(0, 255, 0, 1);

            for (int i = positionNum; i <= max; i++)
            {
                lineRenderer.SetPosition(i, dotsToConnect[positionNum].transform.position);
            }
            positionNum++;
        }
    }
}
