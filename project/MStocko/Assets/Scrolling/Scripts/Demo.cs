﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo : MonoBehaviour {

    public bool set;
    public float timer = 1;
    public int imageIndex = 1;
    public GameObject parentGameObject;

    private TransitionControl reference;

    private void Start()
    {
        set = false;
        reference = parentGameObject.GetComponent<TransitionControl>();
    }

    private void Update()
    {
        if (set)
        {
            timer = -1 * Time.deltaTime;
            if (timer <= 0)
            {
                set = false;
                reference.GlobalTransitionAndScale(imageIndex);
            }
        }
    }
}
