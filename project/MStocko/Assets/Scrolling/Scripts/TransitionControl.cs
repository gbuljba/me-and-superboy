﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransitionControl : MonoBehaviour {
    
    public int startImageIndex = 10;
    public float startImageReturn = 1.5f;
    public float timeToScale = 2f;
    public float timeToTransition = 2f;
    public float delay = 0.5f;
    public Image parentGameObject;
    public Image[] imageArray;
    public GameObject arrow;

    private bool go;
    private bool moving;
    private bool scaling;
    private int ImageIAmOn;
    private int ImageIWantToGoTo;
    private Vector2 location;
    private float timer;
    private float timer2;
    private List<Vector2> anchorPoints = new List<Vector2>();
    private float scale;
    private Vector3 startingScale;
    private Vector3 wantedScale;
    private Vector2 endingVector;
    private int flag;
    private int storeImage;
    private float storeScale;
    private float lastScale;
    
    void Start ()
    {
        GlobalSet(startImageIndex);
    }
	

	void Update ()
    {
        if (go)
        {
            Scheduler();
        }
        if (moving)
        {
            Transition();
        }
        if (scaling)
        {
            Scale();
        }
	}

    public void GlobalSet(int startImage)
    {
        go = false;
        flag = 0;
        lastScale = startImageReturn;
        moving = false;
        scaling = false;
        arrow.SetActive(false);

        scale = parentGameObject.GetComponent<RectTransform>().localScale.x;

        int init = 1;
        foreach (Image i in imageArray)
        {
            i.GetComponent<ImageIdentifier>().indexOfImage = init++;
        }

        ImageIAmOn = startImage;
        parentGameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        GetAnchorPoints();
        parentGameObject.GetComponent<RectTransform>().anchoredPosition = scale * (anchorPoints[0] - anchorPoints[startImage]);
        anchorPoints.Clear();
    }
    
    public float GlobalTransitionAndScale(int imageIndex)
    {
        storeScale = imageArray[imageIndex - 1].GetComponent<ImageIdentifier>().zoomForMe;
        storeImage = imageIndex;
        arrow.SetActive(true);
        return 2 * timeToScale + timeToTransition + delay;
    }

    public void Scheduler()
    {
        switch (flag) {
            case 0:
                GetAnchorPoints();
                ScaleReady(GetReturnScale());
                scaling = true;
                flag = 1;
                break;
            case 1:
                if (!scaling) flag = 2;
                break;
            case 2:
                GetAnchorPoints();
                TransitionReady(storeImage);
                moving = true;
                flag = 3;
                break;
            case 3:
                if (!moving) flag = 4;
                break;
            case 4:
                GetAnchorPoints();
                ScaleReady(storeScale);
                scaling = true;
                flag = 5;
                break;
            case 5:
                if (!scaling) flag = 6;
                break;
            case 6:
                lastScale = storeScale;
                go = false;
                flag = 0;
                break;
        }
    }

    public float GetReturnScale() {
        return 1 / lastScale;
    }

    public void GetAnchorPoints()
    {
        anchorPoints.Add(parentGameObject.GetComponent<RectTransform>().anchoredPosition);
        foreach (Image i in imageArray)
        {
            anchorPoints.Add(i.GetComponent<RectTransform>().anchoredPosition);
        }
    }

    public void ArrowPressed()
    {
        arrow.SetActive(false);
        go = true;
        GetComponentInParent<Main>().MoveToNext();
    }

    public void TransitionReady(int imageIndexNext) {
        timer = timeToTransition;
        ImageIWantToGoTo = imageIndexNext;
        location = anchorPoints[0] + scale * (anchorPoints[ImageIAmOn] - anchorPoints[ImageIWantToGoTo]);
    }

    public void Transition()
    {
        timer -= 1 * Time.deltaTime;
        float interpoliate = 0.5f * Mathf.Cos((timeToTransition - timer) / timeToTransition * Mathf.PI - Mathf.PI) + 0.5f;
        parentGameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(anchorPoints[0], location, interpoliate);
        if (timer <= 0)
        {
            anchorPoints.Clear();
            ImageIAmOn = ImageIWantToGoTo;
            moving = false;
        }
    }

    public void ScaleReady(float scaleRatio) {
        timer2 = timeToScale;
        startingScale = parentGameObject.GetComponent<RectTransform>().localScale;
        wantedScale = startingScale * scaleRatio;
        endingVector = -(anchorPoints[ImageIAmOn] * scale * scaleRatio);
    }

    public void Scale()
    {
        timer2 -= 1 * Time.deltaTime;
        float interpoliate = 0.5f * Mathf.Cos((timeToScale - timer2) / timeToScale * Mathf.PI - Mathf.PI) + 0.5f;
        parentGameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(anchorPoints[0], endingVector, interpoliate);
        parentGameObject.GetComponent<RectTransform>().localScale = Vector3.Lerp(startingScale, wantedScale, interpoliate);
        if (timer2 <= 0)
        {
            anchorPoints.Clear();
            scale = parentGameObject.GetComponent<RectTransform>().localScale.x;
            scaling = false;
        }
    }
}