﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Adress of every image in scene.
/// </summary>

public class ImageIdentifier : MonoBehaviour {

    public int indexOfImage;
    public float zoomForMe;
}