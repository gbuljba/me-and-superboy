﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputPrompt : MonoBehaviour {

    private static Character character;
    private static bool response = false;
    private float timeStart;
    private Character temp;
    public bool optionSent = false;
    private int optionChosen;
    private bool answered = false;

    public static void Prompt(Character character)
    {
        InputPrompt.character = character;

        List<InteractionTransition> transitions = character.getCurrentState().getTransitions();
        Main.textPanel.SetActive(true);
        
        int i = 0;
        foreach (InteractionTransition trans in transitions)
        {
            print(trans.getLine());
            Options o = Main.textPanel.GetComponent<Options>();
            switch(i)
            {
                case 0:
                    o.option1 = trans.getLine();
                    break;
                case 1:
                    o.option2 = trans.getLine();
                    break;
                case 2:
                    o.option3 = trans.getLine();
                    break;
            }
            i++;
        }
    }

    
    private void Update()
    {
        if (character == null) return;

        int option = 0;
        bool keyDown = false;
        if(Input.GetKeyDown(KeyCode.Alpha0))
        {
            option = 0;
            keyDown = true;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            option = 1;
            keyDown = true;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            option = 2;
            keyDown = true;
        }
        
        if(keyDown)
        {
            keyDown = false;
            response = true;
            timeStart = Time.time;
            temp = character;
            character = null;
            answered = true;
            temp.ActivateResponseBubble(option);
            //StartCoroutine(ReturnToInteract(false, 3f, option));
        }

        if(optionSent)
        {
            optionSent = false;
            response = true;
            timeStart = Time.time;
            temp = character;
            character = null;
            answered = true;
            temp.ActivateResponseBubble(optionChosen);
            //StartCoroutine(ReturnToInteract(false, 3f, optionChosen));
        }
    }
    
    public void ReturnToInteract()
    {
        if (!Main.interaction) return;
        if (!answered) return;
        answered = false;
        temp.Interact(optionChosen);
    }

    public void SendOption(int option)
    {
        optionSent = true;
        optionChosen = option;

        Main.textPanel.GetComponent<Options>().option1 = "";
        Main.textPanel.GetComponent<Options>().option2 = "";
        Main.textPanel.GetComponent<Options>().option3 = "";
        Main.textPanel.SetActive(false);
    }
}
