﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionState
{
    private int stateId;
    private string currentLine;
    private List<InteractionTransition> nextStates = new List<InteractionTransition>();

    public InteractionState(int stateId, string currentLine)
    {
        this.stateId = stateId;
        this.currentLine = currentLine;
    }

    public int getStateId()
    {
        return stateId;
    }

    public string getCurrentLine()
    {
        return currentLine;
    }

    public List<InteractionTransition> getTransitions()
    {
        return nextStates;
    }
} 

public class InteractionTransition
{
    public string line;
    public int bubbleId;
    public InteractionState nextState;

    public InteractionTransition(string line, int bubbleId, InteractionState nextState)
    {
        this.line = line;
        this.bubbleId = bubbleId;
        this.nextState = nextState;
    }

    public string getLine()
    {
        return line;
    }

    public int getBubbleId()
    {
        return bubbleId;
    }

    public InteractionState getNextState()
    {
        return nextState;
    }
}

public class Character : MonoBehaviour {

    public TextAsset taStates;
    public TextAsset taTransitions;
    public int initialStateId;
    public Panel panel;
    public float waitTime = 1f;

    private List<InteractionState> states = new List<InteractionState>();
    private InteractionState currentState;

    private void Start()
    {
        InitializeStates();
        currentState = getStateWithId(initialStateId);
    }

    private void InitializeStates()
    {
        string[] lines = taStates.text.Split('\n');
        foreach(string s in lines)
        {
            InteractionState newState = new InteractionState(int.Parse(s.Substring(0, 3)), s.Substring(4));
            states.Add(newState);
        }

        lines = taTransitions.text.Split('\n');
        foreach (string s in lines)
        {
            int state1 = int.Parse(s.Substring(0, 3));
            int state2 = int.Parse(s.Substring(4, 3));
            int bubbleId = int.Parse(s.Substring(8, 3));
            string line = s.Substring(12);

            InteractionState firstState = getStateWithId(state1);
            InteractionState secondState = getStateWithId(state2);
            firstState.getTransitions().Add(new InteractionTransition(line, bubbleId, secondState));
        }
    }

    private InteractionState getStateWithId(int id)
    {
        foreach (InteractionState intState in states)
        {
            if (intState.getStateId() == id)
            {
                return intState;
            }
        }

        return null;
    }

    public void Interact(int option)
    {
        Main.interaction = true;
        Main.waiting = false;

        if(option != -1)
        {
            
            //ActivateResponseBubble(option);

            currentState = currentState.getTransitions()[option].getNextState();
            if(currentState.getStateId() >= 100 && currentState.getStateId() < 900)
            {
                GameEvent ge = new GameEvent();
                ge.eventType = GameEvent.GameEventType.STATE_REACHED;
                ge.name = gameObject.name;
                ge.id = currentState.getStateId();
                //ge.panelId = transform.root.GetComponent<Panel>().getId();
                Main.gameEvents.Add(ge);
            }
        }

        string line = currentState.getCurrentLine();
        if (line != "-")
        {
            Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
            foreach(Object o in objects)
            {
                if (o.name == "Bubble" + currentState.getStateId())
                {
                    ((GameObject)o).SetActive(true);
                }
            }
            print(line);
        }

        if(currentState.getStateId() > 100)
        {
            Main.S.events.Add(currentState.getStateId());
        }

        if(currentState.getStateId() < 900)
        {
            InputPrompt.Prompt(this);
        } 
        else
        {
            Main.interaction = false;
            StartCoroutine(panel.ExecuteNextCommand(false, 0f));
        }
    }

    public InteractionState getCurrentState()
    {
        return currentState;
    }

    public void ActivateResponseBubble(int option)
    {
        int bubbleId = currentState.getTransitions()[option].getBubbleId();

        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name == "Bubble" + bubbleId)
            {
                ((GameObject)o).SetActive(true);
            }
        }
    }
}
