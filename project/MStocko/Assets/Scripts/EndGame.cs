﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour {

    private bool clicked = true;
    private GameObject replay;
    private GameObject print;

    public void Awake()
    {
        GameObject.Find("Text").GetComponent<Text>().CrossFadeAlpha(0f, 0f, false);
        StartCoroutine(ShowText(false, 2f));

        replay = GameObject.Find("Replay");
        print = GameObject.Find("Print");

        replay.SetActive(false);
        print.SetActive(false);
    }

    public void Replay()
    {
        GameObject.Find("Replay").SetActive(false);
        GameObject.Find("Print").SetActive(false);

        Void.S.SetActive(true);
        Color c = Void.S.GetComponent<Image>().color;
        c.a = 1f;
        Void.S.GetComponent<Image>().color = c;
        Void.S.GetComponent<CanvasRenderer>().SetAlpha(0.01f);
        Void.S.GetComponent<Image>().CrossFadeAlpha(1f, 1f, false);
        GameObject.Find("Text").GetComponent<Text>().CrossFadeAlpha(0f, 1f, false);

        StartCoroutine(LoadGame(false, 2.5f));
    }

    public IEnumerator LoadGame(bool status, float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("StartScreen");
    }

    public IEnumerator ShowText(bool status, float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject.Find("Text").GetComponent<Text>().CrossFadeAlpha(1f, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        clicked = false;
    }

    public void Clicked()
    {
        if (clicked) return;
        clicked = true;
        replay.SetActive(true);
        print.SetActive(true);
        GameObject.Find("Replay").GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
        GameObject.Find("Print").GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
        GameObject.Find("Replay").GetComponent<Image>().CrossFadeAlpha(1f, 0.5f, false);
        GameObject.Find("Print").GetComponent<Image>().CrossFadeAlpha(1f, 0.5f, false);
    }
}
