﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLineNew : MonoBehaviour
{

    #region variables

    public Character character;

    //linija koja crta
    private LineRenderer lineRenderer;
    private int linePosition = 0;

    private bool finishedConnecting = false;

    //objekti koje treba pospajati, njihov broj(+1) i u kojem smjeru je počeo crtati
    public GameObject[] dotsToConnect;
    private int max, startingDot;

    //final variables za provjeru
    private int mathed;
    private bool goode;

    //objekti koji su pospajani pomoću miša
    private List<GameObject> selectedDots = new List<GameObject>();

    //početna točka crtanja je napravljena
    private bool startingDotSelected = false;
    //početna točka je ponovno odabrana
    private bool startingDotReselected = false;

    //označava da li se povezivanje točaka smije događati nakon što je ponovno vezana prva točka
    [Tooltip("Dot connecting may continue after starting dot has been re-selected?")]
    public bool continueDotConnecting = false;

    //varijable za provjeru položaja miša
    Vector3 mousePos;
    Vector2 mousePos2D;

    RaycastHit2D hit;

    #endregion

    // Use this for initialization
    void Start()
    {
        //inicijalizacija koliko točaka treba biti
        max = dotsToConnect.Length + 1;
        startingDot = -1;
        //inicijalizacija linije
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = max;
        lineRenderer.startWidth = .2f;
        lineRenderer.endWidth = .2f;
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            ResetStats();
        }

        //kliknuo mišem
        if (Input.GetMouseButtonDown(0))
        {
            //šta je pritisnuto
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos2D = new Vector2(mousePos.x, mousePos.y);

            hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            startingDot = -1;
            linePosition = 0;
            foreach (GameObject obj in dotsToConnect)
            {
                startingDot++;
                if (hit.collider == obj.GetComponent<Collider2D>())
                {
                    AddToCollections(obj);
                    startingDotSelected = true;
                    break;
                }
            }

        }
        
        //sve dok se drži miš
        if (Input.GetMouseButton(0)) {
            //odabrao sam prvu točku
            if(startingDotSelected && !finishedConnecting)
            {
                //gledaj gdje je miš
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos2D = new Vector2(mousePos.x, mousePos.y);
                hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
                lineRenderer.SetPosition(linePosition, mousePos);

                //miš nešto pogodio?
                if (hit.collider)
                {
                    //prešao sam preko početne točke? uvjet da je odabrano više od 2 točke
                    if (hit.collider == selectedDots[0].GetComponent<Collider2D>() && selectedDots.Count > 2 && !startingDotReselected)
                    {
                        AddToCollections(selectedDots[0]);
                        startingDotReselected = true;
                        if (!continueDotConnecting)
                        {
                            finishedConnecting = true;
                        }
                    }
                    else
                    {
                        foreach (GameObject obj in dotsToConnect)
                        {
                            //objekt je već u listi!
                            if (selectedDots.Contains(obj)) continue;
                            //objekt nije u list, dodaj
                            else if (hit.collider == obj.GetComponent<Collider2D>())
                            {
                                AddToCollections(obj);
                                break;
                            }
                        }
                    }
                    if (selectedDots.Count == max)
                    {
                        finishedConnecting = true;
                    }
                }
            }
        }

        //pustio klik miša - kraj spajanja
        if (Input.GetMouseButtonUp(0))
        {
            if (selectedDots.Count != max)
            {
                ResetStats();
            } else
            {
                goode = true;

                //selectedDots[0] == dotsToConnect[startingDot] - hint
                //gledam iduću odabranu točku - mora biti točka koja je ispred ili iza početne
                //ta točka je pozicija = cjelobrojni ostatak djeljenja brojem točaka (npr 12%12 = element [0])
                //ako valja, gledam za točke 3 do max
                mathed = ((startingDot + 1) % (max - 1));
                if (selectedDots[1] == dotsToConnect[mathed])
                {
                    for (int i = 2; i < max; i++)
                    {
                        mathed = (mathed+1) % (max-1);
                        if (selectedDots[i] == dotsToConnect[mathed]) { continue; }
                        else
                        {
                            goode = false;
                            break;
                        }
                    }
                } else
                {
                    goode = false;
                }

                //provjera counter clockwise jedino ako nije clockwise
                if (!goode)
                {
                    goode = true;
                    //početna točka je bila 0 -> iduća je onda zadnja
                    if (startingDot == 0) { mathed = max - 2; } //-2 jer je max = length+1, a zadnji je length-1
                    else { mathed = ((startingDot - 1) % (max - 1)); }
                    if (selectedDots[1] == dotsToConnect[mathed])
                    {
                        for (int i = 2; i < max; i++)
                        {
                            mathed--;
                            if (mathed < 0) { mathed = max - 2; }
                            if (selectedDots[i] == dotsToConnect[mathed]) { continue; }
                            else
                            {
                                goode = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        goode = false;
                    }
                }

                if (goode)
                {
                    character.Interact(-1);
                    selectedDots[0].GetComponent<MeshRenderer>().material.color = new Color(0, 255, 0, 1);
                }
                else
                {
                    ResetStats();
                }
            }
        }
    }

    private void ResetStats()
    {
        if (selectedDots.Count > 0)
        {
            selectedDots[0].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 1);
        }
        startingDotSelected = false;
        startingDotReselected = false;
        finishedConnecting = false;
        startingDot = -1;
        selectedDots.Clear();
        lineRenderer.positionCount = 0;
        lineRenderer.positionCount = max;
    }

    private void AddToCollections(GameObject obj)
    {
        selectedDots.Add(obj);
        for (int i = linePosition; i < max; i++)
        {
            lineRenderer.SetPosition(i, obj.transform.position);
        }
        linePosition++;
    }
}
