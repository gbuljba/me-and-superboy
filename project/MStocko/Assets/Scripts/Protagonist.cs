﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Protagonist : MonoBehaviour {

    public static Protagonist S;

    [SerializeField]
    private List<Item> inventory;
    private Animator anim;

    public List<Item> getInventory()
    {
        return inventory;
    }

    void Awake()
    {
        S = this;
        anim = GetComponent<Animator>();
        anim.speed = 0;
    }

    public bool hasItemWithId(int id)
    {
        foreach (Item i in inventory)
        {
            if (i.getSlotId() == id)
            {
                return true;
            }
        }
        return false;
    }

    public Item takeOutItemWithId(int id)
    {
        foreach (Item i in inventory)
        {
            if (i.getSlotId() == id)
            {
                inventory.Remove(i);
                return i;
            }
        }
        return null;
    }

    public Item getItemWithId(int id)
    {
        foreach (Item i in inventory)
        {
            if (i.getSlotId() == id)
            {
                return i;
            }
        }
        return null;
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(anim.speed == 0)
            {
                anim.speed = 1;
                this.GetComponent<Rigidbody>().velocity = Vector3.one;
            } else
            {
                anim.speed = 0;
                this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            }
        }
    }
}
