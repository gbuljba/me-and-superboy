﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Void : MonoBehaviour {

    public static GameObject S;

	void Awake() {
        S = gameObject;
        GetComponent<Image>().CrossFadeAlpha(0f, 1.0f, false);
        StartCoroutine(TurfOff(false, 1f));
	}

    public IEnumerator TurfOff(bool status, float delay)
    {
        yield return new WaitForSeconds(delay);
        S.SetActive(false);
    }
}
