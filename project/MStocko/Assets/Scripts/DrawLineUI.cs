﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class DrawLineUI : MonoBehaviour
{

    #region variables

    #region privateVars
    //linija koja crta
    private LineRenderer lineRenderer;
    private int linePosition = 0;

    private bool finishedConnecting = false;

    //final variables za provjeru
    private int mathed;
    private bool goode;

    //objekti koji su pospajani pomoću miša
    private List<GameObject> selectedDots = new List<GameObject>();
    //objekti koje služim za slučaj Line_Circle
    private List<GameObject> linear = new List<GameObject>();
    private List<GameObject> circular = new List<GameObject>();
    private List<GameObject> selectedLinear = new List<GameObject>();
    private List<GameObject> selectedCircular = new List<GameObject>();
    private bool startedWithLinear = false;

    //početna točka crtanja je napravljena
    private bool startingDotSelected = false;
    //početna točka je ponovno odabrana
    private bool startingDotReselected = false;

    //varijable za provjeru položaja miša
    Vector3 mousePos;
    Vector2 mousePos2D;

    RaycastHit2D hit;
    #endregion privateVars

    #region publicVars
    //objekti koje treba pospajati, njihov broj(+1) i u kojem smjeru je počeo crtati
    [Header("(please drag-drop them as a drawing order)")]
    public GameObject[] dotsToConnect;
    private int max, startingDot; //related to dotsToConnect

    [Header("(Canvas in Screen Space - Camera + a camera on it)")]
    public Canvas canvas;

    //označava da li se povezivanje točaka smije događati nakon što je ponovno vezana prva točka
    [Header("Connect styles")]

    [ConditionalHide(new string[] { "Line_CircularConnecting", "LinearConnecting" }, false, true)]
    public bool CircularConnecting = true;

    [ConditionalHide(new string[] { "CircularConnecting", "LinearConnecting" }, false, true)]
    [Tooltip("Dot connecting has a linear and circular part (e.g. spyglass). PLEASE ORDER AS NAME IMPLIES; FIRST DOTS THAT ARE FROM THE LINEAR PART, THEN THE CIRCLE ONES. NO DUPLICATES.")]
    public bool Line_CircularConnecting = false;

    //[ConditionalHide("Line_CircularConnecting", true)]
    //[Tooltip("Exclude the dot that is in the circle part AND the first dot of this list MUST BE the starting dot (or ending dot, if you start drawing the circle first)")]
    //public GameObject[] linearDots;

    [Tooltip("Which dot from the circle is used twice")]
    [ConditionalHide("Line_CircularConnecting", true)]
    public GameObject duplicatedCircleDot;

    [ConditionalHide(new string[] { "CircularConnecting", "Line_CircularConnecting" }, false, true)]
    public bool LinearConnecting = false;
    #endregion publicVars

    #endregion variables

    public Character character;
    public bool speechBubble = false;

    // Use this for initialization
    void Start()
    {
        //inicijalizacija koliko točaka treba biti
        max = dotsToConnect.Length + 1;
        //ako je linearno povezivanje, nema "duplicirane" točke
        if (LinearConnecting)
        {
            max--;
        }
        startingDot = -1;
        //inicijalizacija linije
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = max;
        lineRenderer.startWidth = .2f;
        lineRenderer.endWidth = .2f;

        //linecircular - to simplify it for myself, there's only 4 ways to create the object: (starting from the linear or starting from the circle) + 2 directions (in circle part)
        if (Line_CircularConnecting)
        {
            goode = false;
            foreach (GameObject obj in dotsToConnect)
            {
                if (obj == duplicatedCircleDot) goode = true;
                if (!goode)
                {
                    linear.Add(obj);
                } else
                {
                    circular.Add(obj);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //FOR TESTING ON PC
        if (Input.GetKey(KeyCode.R))
        {
            ResetStats();
        }

        //kliknuo mišem
        if (Input.GetMouseButtonDown(0))
        {
            //šta je pritisnuto
            mousePos = Input.mousePosition;
            mousePos.z = -10f;
            mousePos = canvas.worldCamera.ScreenToWorldPoint(mousePos);
            mousePos2D = new Vector2(-mousePos.x, -mousePos.y);

            hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            startingDot = -1;
            linePosition = 0;
            if (!Line_CircularConnecting)
            {
                foreach (GameObject obj in dotsToConnect)
                {
                    startingDot++;
                    if (hit.collider == obj.GetComponent<Collider2D>())
                    {
                        AddToCollections(selectedDots, obj);
                        startingDotSelected = true;
                        break;
                    }
                }
            }
            else
            {
                foreach (GameObject obj in linear)
                {
                    if (hit.collider == obj.GetComponent<Collider2D>())
                    {
                        AddToCollections(selectedLinear, obj);
                        startedWithLinear = true;
                    }
                }
                if (linear.Count != 0)
                {
                    foreach(GameObject obj in circular)
                    {
                        if (hit.collider == obj.GetComponent<Collider2D>())
                        {
                            AddToCollections(selectedCircular, obj);
                        }
                    }
                }
                if (selectedCircular.Count > 0 || selectedLinear.Count > 0)
                {
                    startingDotSelected = true;
                }
            }
        }

        //sve dok se drži miš
        if (Input.GetMouseButton(0))
        {
            //odabrao sam prvu točku
            if (startingDotSelected && !finishedConnecting)
            {
                //gledaj gdje je miš
                mousePos = Input.mousePosition;
                mousePos.z = -10f;
                mousePos = canvas.worldCamera.ScreenToWorldPoint(mousePos);
                mousePos2D = new Vector2(-mousePos.x, -mousePos.y);

                hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
                lineRenderer.SetPosition(linePosition, mousePos2D);

                //miš nešto pogodio?
                if (hit.collider)
                {
                    if (CircularConnecting)
                    {
                        //prešao sam preko početne točke? (uvjet da je odabrano više od 2 točke)
                        if (hit.collider == selectedDots[0].GetComponent<Collider2D>() && selectedDots.Count > 2 && !startingDotReselected)
                        {
                            AddToCollections(selectedDots, selectedDots[0]);
                            startingDotReselected = true;
                            finishedConnecting = true;
                        }
                        else
                        {
                            checkList(selectedDots, 0);
                        }
                    }
                    else if (Line_CircularConnecting)
                    {
                        if (hit.collider == duplicatedCircleDot.GetComponent<Collider2D>() && selectedCircular.Count > 2 && !startingDotReselected)
                        {
                            AddToCollections(selectedCircular, duplicatedCircleDot);
                            startingDotReselected = true;
                        }
                        else
                        {
                            //checking with true lists linear|circular because if the object already IS in the SELECTED list versions of linear|circular, it won't be added anyways.
                            if (linear.Contains(hit.collider.gameObject))
                            {
                                checkList(selectedLinear, 1);
                            } else if (circular.Contains(hit.collider.gameObject))
                            {
                                checkList(selectedCircular, 2);
                            }
                        }
                    }
                    else //LinearConnecting
                    {
                        checkList(selectedDots, 0);
                    }

                    //završavanje je ako je "dovoljno" točaka povezano
                    if (selectedDots.Count == max || (selectedLinear.Count + selectedCircular.Count) == max)
                    {
                        finishedConnecting = true;
                    }
                }
            }
        }

        //pustio klik miša - kraj spajanja
        if (Input.GetMouseButtonUp(0))
        {
            if (selectedDots.Count != max && (selectedLinear.Count + selectedCircular.Count) != max)
            {
                ResetStats();
            }
            else
            {
                goode = true;

                if (CircularConnecting)
                {
                    checkCircle(selectedDots, startingDot, new List<GameObject>(dotsToConnect));
                }
                else if (Line_CircularConnecting)
                {
                    goode = false;

                    if (startedWithLinear)
                    {
                        if (dotsToConnect[0] == selectedLinear[0])
                        {
                            for (int i = 1; i < selectedLinear.Count; i++)
                            {
                                if (selectedLinear[i] == linear[i]) goode = true;
                                else goode = false;
                            }
                            if (goode) //linearni dio je dobar
                            {
                                checkCircle(selectedCircular, 0, circular);
                            }
                        }
                    }
                    else //it did not start with linear
                    {
                        goode = true;
                        checkCircle(selectedCircular, 0, circular);
                        if (goode)
                        {
                            var m = selectedLinear.Count - 1;
                            for (int i = 0; i < selectedLinear.Count; i++)
                            {
                                if (selectedLinear[i] == linear[m - i]) continue;
                                goode = false;
                            }
                        }
                    }

                }
                else //LinearConnecting
                {
                    goode = true;
                    if (selectedDots[0] == dotsToConnect[0])
                    {
                        for(int i = 1; i < max; i++)
                        {
                            if (selectedDots[i] == dotsToConnect[i]) continue;
                            goode = false;
                        }
                    } else if (selectedDots[0] == dotsToConnect[max-1])
                    {
                        var m = max - 1;
                        for (int i = 1; i < max; i++)
                        {
                            if (selectedDots[i] == dotsToConnect[m - i]) continue;
                            goode = false;
                        }
                    } else
                    {
                        goode = false;
                    }
                }

                if (goode)
                {
                    if (selectedDots.Count > 0)
                    {
                        selectedDots[0].GetComponent<MeshRenderer>().material.color = new Color(0, 255, 0, 1);
                    } else if (selectedLinear.Count > 0)
                    {
                        selectedLinear[0].GetComponent<MeshRenderer>().material.color = new Color(0, 255, 0, 1);
                        Done();
                    }
                }
                else
                {
                    ResetStats();
                }
            }
        }
    }

    private void ResetStats()
    {
        if (selectedDots.Count > 0)
        {
            selectedDots[0].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 1);
        }
        if (selectedLinear.Count > 0)
        {
            selectedLinear[0].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 1);
        }
        startingDotSelected = false;
        startingDotReselected = false;
        finishedConnecting = false;
        startedWithLinear = false;
        startingDot = -1;
        selectedDots.Clear();
        selectedLinear.Clear();
        selectedCircular.Clear();
        lineRenderer.positionCount = 0;
        lineRenderer.positionCount = max;
    }

    //version - (circular || linear) = 0, line-circular = (1 for adding to linear, 2 for adding to circular)
    private void checkList(List<GameObject> list, int version)
    {
        if (version == 0)
        {
            foreach (GameObject obj in dotsToConnect)
            {
                //objekt je već u listi!
                if (list.Contains(obj)) continue;
                //objekt nije u list, dodaj
                else if (hit.collider == obj.GetComponent<Collider2D>())
                {
                    AddToCollections(list, obj);
                    break;
                }
            }
        }
        else if (version == 1 || version == 2)
        {
            var which = (version == 1 ? linear : circular);
            foreach (GameObject obj in which)
            {
                //objekt je već u listi!
                if (list.Contains(obj)) continue;
                //objekt nije u list, dodaj
                else if (hit.collider == obj.GetComponent<Collider2D>())
                {
                    AddToCollections(list, obj);
                    break;
                }
            }
        } 
    }

    private void AddToCollections(List<GameObject> list, GameObject obj)
    {
        list.Add(obj);
        for (int i = linePosition; i < max; i++)
        {
            lineRenderer.SetPosition(i, obj.transform.position);
        }
        linePosition++;
    }

    private void checkCircle(List<GameObject> selectedDots, int startingDot, List<GameObject> dotsToConnect) 
    {
        //selectedDots[0] == dotsToConnect[startingDot] - hint
        //gledam iduću odabranu točku - mora biti točka koja je ispred ili iza početne
        //ta točka je pozicija = cjelobrojni ostatak djeljenja brojem točaka (npr 12%12 = element [0])
        //ako valja, gledam za točke 3 do max
        mathed = ((startingDot + 1) % dotsToConnect.Count);
        if (selectedDots[0].gameObject == dotsToConnect[startingDot].gameObject)
        {
            if (selectedDots[1].gameObject == dotsToConnect[mathed].gameObject)
            {
                for (int i = 2; i < dotsToConnect.Count; i++)
                {
                    mathed = (mathed + 1) % dotsToConnect.Count;
                    if (selectedDots[i].gameObject == dotsToConnect[mathed].gameObject) { continue; }
                    else
                    {
                        goode = false;
                        break;
                    }
                }
            }
            else
            {
                goode = false;
            }

            //provjera counter clockwise jedino ako nije clockwise
            if (!goode)
            {
                goode = true;
                //početna točka je bila 0 -> iduća je onda zadnja
                if (startingDot == 0) { mathed = dotsToConnect.Count - 1; } //-2 jer je max = length+1, a zadnji je length-1
                else { mathed = ((startingDot - 1) % dotsToConnect.Count); }
                if (selectedDots[1].gameObject == dotsToConnect[mathed].gameObject)
                {
                    for (int i = 2; i < dotsToConnect.Count; i++)
                    {
                        mathed--;
                        if (mathed < 0) { mathed = dotsToConnect.Count - 1; }
                        if (selectedDots[i].gameObject == dotsToConnect[mathed].gameObject) { continue; }
                        else
                        {
                            goode = false;
                            break;
                        }
                    }
                }
                else
                {
                    goode = false;
                }

                if (goode) Done();
            }
        }
        else
        {
            goode = false;
        }
    }

    private void Done()
    {
        this.transform.parent.gameObject.SetActive(false);
        Main.waiting = false;
        if (speechBubble)
        {
            character.Interact(-1);
        }
        else
        {
            Main.S.ReturnControl();
        }
    }
}
