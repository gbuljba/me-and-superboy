﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour {

    [SerializeField]
    private int id;
    private Item item;
    
    /*public List<Item> acceptableItems;

    void Start()
    {
        acceptableItems = new List<Item>();
        acceptableItems = transform.root.GetComponent<Panel>().GetItemsWithId(id);
        Vector2 size = Vector2.zero;
        foreach(Item i in acceptableItems)
        {
            if(i.transform.localScale.x > size.x)
            {
                size.x = i.transform.localScale.x;
            }
            if (i.transform.localScale.y > size.y)
            {
                size.y = i.transform.localScale.y;
            }
        }
        print(size);
        GetComponent<BoxCollider>().size = size;
    }*/

    void OnMouseEnter()
    {
        if(Protagonist.S.hasItemWithId(id))
        {
            if(item == null)
            {
                item = Protagonist.S.getItemWithId(id);
                item.transform.position = this.transform.position;
                item.setAlpha(0.3f);
            }
            item.gameObject.SetActive(true);
        }
    }

    void OnMouseExit()
    {
        if(item != null)
        {
            item.gameObject.SetActive(false);
        }
    }

    void OnMouseDown()
    {
        if (!Protagonist.S.hasItemWithId(id)) return;
        item = Protagonist.S.takeOutItemWithId(id);
        if(item != null)
        {
            item.setAlpha(1f);
            GameEvent ge = new GameEvent();
            ge.eventType = GameEvent.GameEventType.ITEM_PUT_DOWN;
            ge.name = item.getItemName();
            ge.id = id;
            //ge.panelId = transform.root.GetComponent<Panel>().getId();
            Main.gameEvents.Add(ge);
            Destroy(gameObject);
        }
    }
}
 