﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Option : MonoBehaviour {

    public Panel panel;
    public int nextPanel;
    public TextAsset newCommands;
    
    public void Send()
    {
        panel.ChangeNextPanel(nextPanel);
        if(newCommands != null)
        {
            panel.SwapCommands(newCommands);
        }
    }
}
