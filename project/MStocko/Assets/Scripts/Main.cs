﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEvent
{
    public enum GameEventType {
        ITEM_PICKED_UP,
        ITEM_PUT_DOWN,
        STATE_REACHED
    }

    public GameEventType eventType;
    public string name;
    public int id;
    public int panelId;
}

[System.Serializable]
public class Main : MonoBehaviour {

    public static Main S;

    public static List<GameEvent> gameEvents = new List<GameEvent>();
    public static GameObject textPanel;
    public static bool interaction = false;
    public static bool moving = false;
    public static bool waiting = false;
    public static bool firstPlaythrough = true;

    public List<int> events = new List<int>();
    public int firstPanelId;
    private int currentPanelId;
    private int newPanelId;
    private bool changing = false;
    private float timeForNext;
    private GameObject newPanel;
    private bool ending = false;

    public void Awake()
    {
        S = this;

        print("Pokrećem prvi");
        GameObject first = FindPanel(firstPanelId);
        currentPanelId = firstPanelId;
        first.SetActive(true);

        StartCoroutine(first.GetComponent<Panel>().ExecuteNextCommand(false, 1f));

        textPanel = GameObject.Find("TextPanel");
        textPanel.SetActive(false);
    }

    public void Update()
    {
        if(changing)
        {           
            newPanel = FindPanel(newPanelId);
            newPanel.SetActive(true);
            currentPanelId = newPanelId;
            int idForTheNextPanel = currentPanelId;
            if(idForTheNextPanel > 100)
            {
                idForTheNextPanel /= 10;
            }
            timeForNext = GameObject.Find("ComicPage_876x576").GetComponent<TransitionControl>().GlobalTransitionAndScale(idForTheNextPanel);

            changing = false;
        }
    }

    public void MoveToNextPanel(int panelId)
    {
        if (panelId != -1)
        {
            print("Prelazim na sljedeći panel: " + panelId);

            newPanelId = panelId;
            changing = true;
            moving = true;
        }
        else
        {
            ending = true;
        }
    }

    public void EndLevel()
    {
        if (!ending) return;

        Void.S.SetActive(true);
        Color c = Void.S.GetComponent<Image>().color;
        c.a = 1f;
        Void.S.GetComponent<Image>().color = c;
        Void.S.GetComponent<CanvasRenderer>().SetAlpha(0.01f);
        Void.S.GetComponent<Image>().CrossFadeAlpha(1f, 2.0f, false);
        StartCoroutine(LoadEnd(false, 3f));
    }

    public IEnumerator LoadEnd(bool status, float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene("EndScreen");
    }

    private IEnumerator EnableClicking(bool status, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        moving = false;
    }

    public void MoveToNext()
    {
        StartCoroutine(EnableClicking(false, timeForNext));
        StartCoroutine(newPanel.GetComponent<Panel>().ExecuteNextCommand(false, timeForNext));
    }

    public void ReturnControl()
    {
        if (interaction || moving) return;
        if (waiting) return;
        StartCoroutine(FindPanel(currentPanelId).GetComponent<Panel>().ExecuteNextCommand(false, 0f));
    }

    public static GameObject FindPanel(int id)
    {
        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name == "Panel" + id)
            {
                return (GameObject)o;
            }
        }

        return null;
    }

    public static void ActivateBubble(int id)
    {
        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name == "Bubble" + id)
            {
                ((GameObject)o).SetActive(true);
            }
        }
    }

    public static void ActivateSpeechBubble(int id)
    {
        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name == "SpeechBubble" + id)
            {
                ((GameObject)o).SetActive(true);
            }
        }
    }

    public static void ActivateHighlight(int id)
    {
        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name == "Highlight" + id)
            {
                ((GameObject)o).SetActive(true);
            }
        }
    }

    public static void DectivateHighlights()
    {
        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name.StartsWith("Highlight"))
            {
                ((GameObject)o).SetActive(false);
            }
        }

        waiting = false;
    }

    public static void ActivateDraw(int id)
    {
        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name == "Draw" + id)
            {
                ((GameObject)o).SetActive(true);
            }
        }
    }

    public static void SpawnObject(int id)
    {
        Object[] objects = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        foreach (Object o in objects)
        {
            if (o.name == "Object" + id)
            {
                ((GameObject)o).SetActive(true);
            }
        }
    }
}
