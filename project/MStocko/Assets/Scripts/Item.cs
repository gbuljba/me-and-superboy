﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    [SerializeField]
    private int slotId;
    [SerializeField]
    private string itemName;

    private bool collectible = true;
    private Color color;

    public int getSlotId()
    {
        return slotId;
    }

    void Start()
    {
        //color = GetComponent<Renderer>().material.color;
    }

    public void OnMouseEnter()
    {
        if (!collectible) return;
        //GetComponent<Renderer>().material.color = Color.red;
        print("Evo ga");
    }

    void OnMouseExit()
    {
        if (!collectible) return;
        GetComponent<Renderer>().material.color = color;
    }

    void OnMouseDown()
    {
        if (!collectible) return;
        GameObject.Find("Protagonist").GetComponent<Protagonist>().getInventory().Add(this);
        GetComponent<Renderer>().material.color = color;
        collectible = false;
        GetComponent<SphereCollider>().enabled = false;
        gameObject.SetActive(false);

        GameEvent ge = new GameEvent();
        ge.eventType = GameEvent.GameEventType.ITEM_PICKED_UP;
        ge.name = itemName;
        //ge.panelId = transform.root.GetComponent<Panel>().getId();
        Main.gameEvents.Add(ge);
    }

    public void setAlpha(float alpha)
    {
        Color c = GetComponent<SpriteRenderer>().material.color;
        c.a = alpha;
        GetComponent<SpriteRenderer>().material.color = c;
    }
    
    public string getItemName()
    {
        return itemName;
    }
}
