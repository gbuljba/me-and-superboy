﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NewScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onEnter()
    {
        Text text = GetComponent<Text>();
        text.fontStyle = FontStyle.Bold;
    }

    public void onExit()
    {
        Text text = GetComponent<Text>();
        text.fontStyle = FontStyle.Normal;
    }

    public void onClick()
    {
        
    }
}
