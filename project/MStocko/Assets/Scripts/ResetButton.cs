﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResetButton : MonoBehaviour {

    //public GameObject object;
    private bool started;
    private float timer;
    private float lerpTimer;
    private Color red = new Color32(255, 70, 70, 255);
    private Color green = new Color32(70, 255, 70, 255);

	// Use this for initialization
	void Start ()
    {
        timer = 0;
        started = false;
        lerpTimer = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            timer = 0;
            started = true;
            lerpTimer = 0;
            GetComponent<Image>().color = red;
        }
        if (Input.GetMouseButton(0) && started)
        {
            timer += Time.deltaTime;
            lerpTimer = timer / 3;
            GetComponent<Image>().color = Color.Lerp(red, green, lerpTimer);
            if (timer > 3)
            {
                started = false;
                //object.GetComponent<SCRIPTNAME>.METHODNAME();
                //Debug.Log("Completed");
                SceneManager.LoadScene("StartScreen");
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            timer = 0;
            started = false;
            lerpTimer = 0;
            GetComponent<Image>().color = red;
        }
	}
}
