﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

    private string[] lines = new string[]
    {
        "Stigao si. 15 minuta kasnije jer je bila gužva na cesti i ulica prema groblju više nije ista.",
        "Činilo se kao da si možda propustio stvar, nisi mogao pogledom naći nikoga niti čuti išta.",
        "Onda si prepoznao njegovu majku koja je sama plakala nad njegovim grobom.",
        "Iako ne voliš misliti da je tako, malo se iznenadila što te vidjela.",
        "I onda je rekla:",
        "Od cijelog mjesta, nitko nije došao, a bio vam je prijatelj iz klupa.",
        "Znaš li da je onaj vaš učitelj mrtav? Tamo mu je grob. Taj je čovjek otjerao mog dječaka iz škole, samo zbog jednog pera..."
    };

    private int counter = 0;
    private bool started = false;
    private GameObject play;

    public void Awake()
    {
        for(int i = 0; i < 7; i++)
        {
            GameObject.Find("Text" + i).GetComponent<Text>().CrossFadeAlpha(0f, 0f, false);
        }

        play = GameObject.Find("Play");
        play.GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
        play.GetComponent<Image>().CrossFadeAlpha(1f, 1f, false);
    }

    public void FadeOut()
    {
        GameObject.Find("Play").SetActive(false);
        StartCoroutine(ShowText(false, 2.0f));
    }

    public IEnumerator LoadGame(bool status)
    {
        GameObject.Find("Background").GetComponent<Image>().CrossFadeColor(Color.black, 2.0f, false, false);
        for (int i = 0; i < 7; i++)
        {
            GameObject.Find("Text" + i).GetComponent<Text>().CrossFadeAlpha(0f, 2.0f, false);
        }
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Gameplay");
    }

    public IEnumerator ShowText(bool status, float delay)
    {
        yield return new WaitForSeconds(delay);
        started = true;
        ShowLine();
    }

    public void ShowLine()
    {
        if (!started) return;

        if (counter == lines.Length)
        {
            started = false;
            StartCoroutine(LoadGame(false));
        }
        else
        {
            GameObject.Find("Text" + counter).GetComponent<Text>().CrossFadeAlpha(1f, 0.5f, false);
            counter++;
        }
    }
}
