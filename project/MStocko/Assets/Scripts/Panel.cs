﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command
{
    public string commandType;
    public int targetId1;
    public int targetId2;

    public Command(string commandType, int targetId1, int targetId2 = 0)
    {
        this.commandType = commandType;
        this.targetId1 = targetId1;
        this.targetId2 = targetId2;
    }

    public string getCommandType()
    {
        return commandType;
    }

    public int getTargetId1()
    {
        return targetId1;
    }

    public int getTargetId2()
    {
        return targetId2;
    }
}

public class Panel : MonoBehaviour {

    public int nextPanel;
    public TextAsset commandsTa;
    private List<Command> commands = new List<Command>();
    private int programCounter;
    private bool allDone = false;
    private bool branching = false;
    private bool emptyPanel = false;
    private bool emptyPanelDone = false;

    public void Awake()
    {
        InitializeCommands();
        programCounter = -1;
    }

    public void Reawake()
    {
        InitializeCommands();
        programCounter = -1;
        allDone = false;
        branching = false;
        emptyPanel = false;
        emptyPanelDone = false;
    }

    private void InitializeCommands()
    {
        string[] lines = commandsTa.text.Split('\n');

        if(lines.Length == 1 && lines[0] == "")
        {
            emptyPanel = true;
            return;
        }

        foreach(string s in lines)
        {
            string[] parameters = s.Split(' ');
            if(parameters[0] != "bool" && parameters[0] != "flag")
            {
                commands.Add(new Command(parameters[0], int.Parse(parameters[1])));
            }
            else
            {
                commands.Add(new Command(parameters[0], int.Parse(parameters[1]), int.Parse(parameters[2])));
            }
        }
    }

    public void SwapCommands(TextAsset newCommands)
    {
        commandsTa = newCommands;
        commands.Clear();
        InitializeCommands();
        programCounter = -1;
        allDone = false;
        branching = false;
        emptyPanel = false;
        emptyPanelDone = false;

        StartCoroutine(ExecuteNextCommand(false, 0f));
    }

    public IEnumerator ExecuteNextCommand(bool status, float delayTime)
    {
        print("Next command, pc=" + programCounter);

        if(emptyPanel)
        {
            emptyPanel = false;
            yield break;
        }

        if(commands.Count == 0)
        {
            Main.S.MoveToNextPanel(nextPanel);
        }

        if (programCounter == commands.Count - 1)
        {
            allDone = true;
            Main.S.MoveToNextPanel(nextPanel);
            yield break;
        }

        if (allDone)
        {
            yield break;
        }

        yield return new WaitForSeconds(delayTime);

        programCounter++;

        switch (commands[programCounter].getCommandType())
        {
            case "bubble":
                Main.ActivateBubble(commands[programCounter].getTargetId1());
                break;
            case "speak":
                Main.waiting = true;
                Main.ActivateSpeechBubble(commands[programCounter].getTargetId1());
                yield break;
            case "bool":
                Main.waiting = true;
                EnableBranching(true);
                Main.ActivateHighlight(commands[programCounter].getTargetId1());
                Main.ActivateHighlight(commands[programCounter].getTargetId2());
                yield break;
            case "draw":
                Main.waiting = true;
                Main.ActivateDraw(commands[programCounter].getTargetId1());
                yield break;
            case "spawn":
                Main.SpawnObject(commands[programCounter].getTargetId1());
                break;
            case "flag":
                if (Main.S.events.Contains(commands[programCounter].getTargetId1()))
                {
                    branching = true;
                    ChangeNextPanel(commands[programCounter].getTargetId2());
                }
                StartCoroutine(ExecuteNextCommand(false, 0f));
                break;
        }

        if (programCounter == commands.Count-1)
        {
            allDone = true;
            Main.S.MoveToNextPanel(nextPanel);
        }

        yield break;
    }

    public void EnableBranching(bool branching)
    {
        this.branching = branching;
    }

    public void ChangeNextPanel(int nextPanel)
    {
        print("Ulazim. " + nextPanel);
        if (!branching) return;
        print("Prihvaćam. " + nextPanel);
        this.nextPanel = nextPanel;
        branching = false;
        Main.DectivateHighlights();
        
        //StartCoroutine(ExecuteNextCommand(false, 0f));
    }
}
