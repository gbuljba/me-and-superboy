﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Options : MonoBehaviour {

    public string option1;
    public string option2;
    public string option3;
    private Text[] texts;
    //private Button[] options;

    // Use this for initialization
    void Start () {
        texts = GetComponentsInChildren<Text>();     

        texts[0].text = option1;
        texts[1].text = option2;
        texts[2].text = option3;

        //options = GetComponentsInChildren<Button>();
        //Text[] optns = options[0].GetComponentsInChildren<Text>();
        //optns[0].text = option1;
    }

    // Update is called once per frame
    void Update () {
        Start();
	}

    public void OnMouseEnter(PointerEventData data)
    {
        
        Debug.Log("OnPointerEnterDelegate called.");
    }
}
